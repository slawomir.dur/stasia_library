from . import data_manager
from . import base_endpoint


class GenresEndpoint(base_endpoint.BaseEndpoint):
    manager = data_manager.GenreManager()

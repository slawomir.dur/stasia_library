from . import data_manager
from . import base_endpoint


class AuthorsEndpoint(base_endpoint.BaseEndpoint):
    manager = data_manager.AuthorManager()

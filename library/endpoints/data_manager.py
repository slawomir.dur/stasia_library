from ..models import Author, Genre, Book
from . import common


class BaseManager:
    model = None
    fields = None

    @property
    def all(self):
        return self.model.objects.all()

    @property
    def all_as_data(self) -> dict:
        objects = []
        for el in self.all:
            d = {'id': el.id}
            for field in self.fields:
                d[field] = getattr(el, field)
            objects.append(d)
        return common.create_collective_data(objects)

    def data_to_one(self, data):
        for field in self.fields:
            if field not in data:
                raise RuntimeError(f'No {field} key in data')
        return self.model(**data)

    def one_to_data(self, obj) -> dict:
        d = {'id': obj.id}
        for field in self.fields:
            d[field] = getattr(obj, field)
        return d

    def update_data(self, id, data):
        obj = self.model.objects.get(pk=id)
        for key, value in data.items():
            if key in self.fields:
                setattr(obj, key, value)
        return obj

    def delete(self, id):
        obj = self.model.objects.get(pk=id)
        obj.delete()


class AuthorManager(BaseManager):
    model = Author
    fields = ['name', 'surname']


class GenreManager(BaseManager):
    model = Author
    fields = ['name']


class BookManager(BaseManager):
    model = Author
    fields = ['name', 'author', 'genre']

def create_collective_data(data: list):
    return {
        'content': [el for el in data],
        'elementCount': len(data)
    }
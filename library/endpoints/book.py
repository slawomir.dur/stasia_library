from . import data_manager
from . import base_endpoint


class BooksEndpoint(base_endpoint.BaseEndpoint):
    manager = data_manager.BookManager

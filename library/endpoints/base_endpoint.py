from rest_framework import views, response, status


class BaseEndpoint(views.APIView):
    manager = None

    def get(self, request, **kwargs):
        return response.Response(
            data=self.manager.all_as_data,
            status=status.HTTP_200_OK
        )

    def post(self, request, format=None):
        obj = self.manager.data_to_one(request.data)
        obj.save()
        return response.Response(
            data=self.manager.one_to_data(obj),
            status=status.HTTP_200_OK
        )

    def put(self, request, pk, format=None):
        obj = self.manager.update_data(pk, request.data)
        obj.save()
        return response.Response(
            data=self.manager.one_to_data(obj),
            status=status.HTTP_200_OK
        )

    def delete(self, request, pk, format=None):
        self.manager.delete(pk)
        return response.Response(
            status=status.HTTP_204_NO_CONTENT
        )
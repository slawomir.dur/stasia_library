NO_ID_ERROR_MSG = 'Request does not contain mandatory "id" parameter'

def construct_error_json(msg):
    return {
        "message": msg
    }
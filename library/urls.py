from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from .endpoints import author, genre, book 

urlpatterns = [
    path('Author/', author.AuthorsEndpoint.as_view(), name='author_get_post'),
    path('Author/<int:pk>/', author.AuthorsEndpoint.as_view(), name='author_get_put_delete'),

    path('Genre/', genre.GenresEndpoint.as_view(), name='genre_get_post'),
    path('Genre/<int:id>/', genre.GenresEndpoint.as_view(), name='genre_get_put_delete'),

    path('Book/', book.BooksEndpoint.as_view(), name='book_get_post'),
    path('Book/<int:id>/', book.BooksEndpoint.as_view(), name='book_get_put_delete'),
]

urlpatterns = format_suffix_patterns(urlpatterns)